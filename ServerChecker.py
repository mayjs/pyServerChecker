import os
import sys
import urllib2
from pyTelegramNotifier.TelegramNotifier import TelegramNotifier

if __name__ == "__main__":
	if len(sys.argv) < 2:
		print "You must enter at least one server to check"
		sys.exit()
	notifier = TelegramNotifier()	

	for server in sys.argv[1:]:
		try: 
			urllib2.urlopen(server).read()
			print "The server " + server + " is online!"
		except urllib2.HTTPError, e:
			msg= "The server " + server + " is offline! (HTTP-Code: %d, Reason: %s)"%(e.code,e.reason)
			print msg
			notifier.sendMessageToAll(msg)
                except urllib2.URLError, e:
			msg = "The server " + server + " is offline! (Reason: %s)"%e.reason
			print msg 
			notifier.sendMessageToAll(msg)
